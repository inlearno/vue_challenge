import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators';
import { indexEventsService } from '@/services';
import { $api } from '@/tools';

@Module({
  namespaced: true,
  stateFactory: true,
})
export default class TestOneStore extends VuexModule {
  list: Array<any> = [];
  loading = false;
  error: string = 'Ошибка!';

  @Mutation
  setLoading(flag: boolean) {
    this.loading = flag;
  }

  @Mutation
  setList(list: Array<any>) {
    this.list = list;
  }

  @Mutation
  setError(text: string) {
    this.error = text;
  }

  @Action
  async fetchEvents() {
    const { commit } = this.context;
    const { data } = await indexEventsService($api).getObjects();
    commit('setList', data);
  }
}
