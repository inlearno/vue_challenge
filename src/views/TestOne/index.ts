import TestOne from './TestOne.vue';
import TestOneStore from './store';

export * from './consts';
export { TestOne, TestOneStore };
