import { indexEventsService } from '@/services';
import { $api } from '@/tools';

const TestTwoStore = {
  namespaced: true,

  state: () => ({
    list: [],
    loading: false,
    error: 'Ошибка!',
  }),

  mutations: {
    setLoading(state, flag) {
      state.loading = flag;
    },

    setList(state, list) {
      state.list = list;
    },

    setError(state, text) {
      state.error = text;
    },
  },

  actions: {
    async fetchEvents({ commit }) {
      const { data } = await indexEventsService($api).getObjects();
      commit('setList', data);
    },
  },
};

export default TestTwoStore;
