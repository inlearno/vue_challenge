import TestTwo from './TestTwo.vue';
import TestTwoStore from './store';

export * from './consts';
export { TestTwo, TestTwoStore };
