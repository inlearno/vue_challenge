import Vue from 'vue';
import Vuex from 'vuex';
import { TestOneStore } from '@/views';
import { TestTwoStore } from '@/views/TestTwo';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    TestOneStore,
    TestTwoStore,
  },
});
