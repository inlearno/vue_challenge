import axios from 'axios';

export const $api = axios.create({
  baseURL: 'https://www.stage.navi.inlearno.info/api/',
});
