import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import { TestOne } from '../views';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'TestOne',
    component: TestOne,
  },
  {
    path: '/two',
    name: 'About',
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/TestTwo/TestTwo.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
