import { AxiosInstance } from 'axios';
import { IndexEventPoints } from './types';
import { parseEventsData } from './utils';

const getIndexEvents = (api: AxiosInstance) => ({
  getObjects: async (
    params = { limit: 18 },
    parser: Function = parseEventsData,
  ) => {
    const { data } = await api.get(IndexEventPoints.GET_INDEX_EVENT, {
      params,
    });
    return { ...data, data: parser(data.data) };
  },
});

const indexEventsService = (api: AxiosInstance) => {
  if (!api) throw new Error('There is no HTTP client');
  return {
    ...getIndexEvents(api),
  };
};

export default indexEventsService;
