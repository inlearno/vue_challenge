const parseEventsData = (data: Array<any>) =>
  data.map((item) => Object.freeze(item));

export default parseEventsData;
