# vue_candidate_test_for_the_best

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Задание

- Выводить данные списка на странице.
- Во время загрузки данных, выводить лоадер.
- Обработать ошибки если api их вернет и вывести их на странцу.
- Если список пуст, выводить сообщение на странице.
- Полученные данные отфильтровать оставив id и name.
- Исправить ошибки если они есть.
- Реализовать в двух вариантах Test One и Test Two
- Вынести блок списка в отделный компонет, для ts
